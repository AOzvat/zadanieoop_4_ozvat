#ifndef GOOGLE_SEARCH_H
#define GOOGLE_SEARCH_H

#include <QtWidgets/QMainWindow>
#include "ui_google_search.h"
#include <QlistWidget>
#include <QUrl>
#include <QLineEdit>
#include <QtNetwork>
#include <QNetworkAccessManager>
#include <QMessageBox>
#include <QDesktopServices>
#include <QRegularExpression>

class Google_search : public QMainWindow
{
	Q_OBJECT

public:
	Google_search(QWidget *parent = 0);
	~Google_search();

	public slots:
		void hladaj();
		void zadaj_link();
		void stiahni_stranku();
		void citaj_stranku();
		void otvor_stranku();
		void otvor();


private:
	Ui::Google_searchClass ui;

	QString google_link = "http://www.google.sk/search?q=";
	QString zadany_text;
	QString hladany_link;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;
};

#endif // GOOGLE_SEARCH_H
