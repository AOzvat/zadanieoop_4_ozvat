/********************************************************************************
** Form generated from reading UI file 'google_search.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GOOGLE_SEARCH_H
#define UI_GOOGLE_SEARCH_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Google_searchClass
{
public:
    QWidget *centralWidget;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QListWidget *listWidget;
    QPushButton *pushButton_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Google_searchClass)
    {
        if (Google_searchClass->objectName().isEmpty())
            Google_searchClass->setObjectName(QStringLiteral("Google_searchClass"));
        Google_searchClass->resize(555, 395);
        centralWidget = new QWidget(Google_searchClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        formLayoutWidget = new QWidget(centralWidget);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 531, 331));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setPointSize(15);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        formLayout->setWidget(1, QFormLayout::FieldRole, label);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(2, QFormLayout::FieldRole, verticalSpacer_2);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        QFont font1;
        font1.setPointSize(10);
        label_2->setFont(font1);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_2);

        lineEdit = new QLineEdit(formLayoutWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setFont(font1);

        formLayout->setWidget(3, QFormLayout::FieldRole, lineEdit);

        pushButton = new QPushButton(formLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QFont font2;
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        pushButton->setFont(font2);

        formLayout->setWidget(4, QFormLayout::FieldRole, pushButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(5, QFormLayout::FieldRole, verticalSpacer);

        listWidget = new QListWidget(formLayoutWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setFont(font1);

        formLayout->setWidget(6, QFormLayout::SpanningRole, listWidget);

        pushButton_2 = new QPushButton(formLayoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setFont(font2);

        formLayout->setWidget(7, QFormLayout::LabelRole, pushButton_2);

        Google_searchClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Google_searchClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 555, 21));
        Google_searchClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Google_searchClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Google_searchClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Google_searchClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Google_searchClass->setStatusBar(statusBar);

        retranslateUi(Google_searchClass);
        QObject::connect(pushButton, SIGNAL(clicked()), Google_searchClass, SLOT(hladaj()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), Google_searchClass, SLOT(otvor()));

        QMetaObject::connectSlotsByName(Google_searchClass);
    } // setupUi

    void retranslateUi(QMainWindow *Google_searchClass)
    {
        Google_searchClass->setWindowTitle(QApplication::translate("Google_searchClass", "Google_search", 0));
        label->setText(QApplication::translate("Google_searchClass", "Google Search", 0));
        label_2->setText(QApplication::translate("Google_searchClass", "Zadajte dotaz vyh\304\276ad\303\241va\304\215u:", 0));
        pushButton->setText(QApplication::translate("Google_searchClass", "H\304\276adaj", 0));
        pushButton_2->setText(QApplication::translate("Google_searchClass", "Otvor", 0));
    } // retranslateUi

};

namespace Ui {
    class Google_searchClass: public Ui_Google_searchClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GOOGLE_SEARCH_H
