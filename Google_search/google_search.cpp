#include "google_search.h"

Google_search::Google_search(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

Google_search::~Google_search()
{

}

void Google_search::hladaj() {
	ui.listWidget->clear();

	zadaj_link();

	stiahni_stranku();

	hladany_link = "";
}

void Google_search::zadaj_link() {
	//Skonstruuje sa link podla, ktoreho budeme hladat
	zadany_text = ui.lineEdit->displayText();
	
	hladany_link = google_link + zadany_text;

	ui.lineEdit->clear();
}

void Google_search::stiahni_stranku() {
	url.setUrl(hladany_link);
	reply = qnam.get(QNetworkRequest(url));
	connect(reply, &QNetworkReply::finished, this, &Google_search::citaj_stranku);
}

void Google_search::citaj_stranku() {
	QString vysledok = reply->readAll();
	QStringList split = vysledok.split(" title=\"http");

	for (int i = 1; i <= 5; i++) {
		QString nieco_medzi = split[i];
		int index = nieco_medzi.indexOf("\"", 0);
		QString link_prvku = "http" + nieco_medzi.mid(0, index);

		QString naslov_prvku = "";
		index = link_prvku.indexOf("/", 9);
		QStringList nieco_ine = vysledok.split("r\"><a href=\"/url?q=" + link_prvku.mid(0,index));
		if (nieco_ine.length() == 1) {
			index = link_prvku.indexOf(".", 0);
			QString prva_cast = link_prvku.mid(index+1);
			index = prva_cast.indexOf("/", 0);
			prva_cast = prva_cast.mid(0, index);
			naslov_prvku = prva_cast + " -> " + zadany_text;
		}
		else {
			nieco_medzi = nieco_ine[1];
			index = nieco_medzi.indexOf("\">", 0);
			naslov_prvku = nieco_medzi.mid(index + 2);
			index = naslov_prvku.indexOf("</a>", 0);
			naslov_prvku = naslov_prvku.mid(0, index);
			naslov_prvku.replace(QString("<b>"), QString(""));
			naslov_prvku.replace(QString("</b>"), QString(""));
		}

		QString cely_prvok = naslov_prvku + "~" + link_prvku;
		ui.listWidget->addItem(cely_prvok);
	}
/*
	QFile co_vravi("Toto_vravi.txt");
	if (co_vravi.open(QFile::WriteOnly | QFile::Text)) {
		QTextStream vravi(&co_vravi);
		vravi << vysledok;
		co_vravi.close();
	}
*/
}

void Google_search::otvor_stranku() {
	//Otvori sa stranka, na ktoru uzivatel knikne
	QString sajt = ui.listWidget->currentItem()->text();
	
	QStringList split = sajt.split('~');
	if (split.length() > 1){
		QDesktopServices::openUrl(QUrl(split[split.length() - 1]));
	}
}

void Google_search::otvor() {
	otvor_stranku();
}